Este repositorio contiene los archivos y ejemplos que se han visto en el curso introductorio de JS y NodeJS.

Documentación de la API disponible en
- https://documenter.getpostman.com/view/3057287/TzskDiBS (web)
- https://www.postman.com/collections/ddedd6be7df0c025c491 (json)