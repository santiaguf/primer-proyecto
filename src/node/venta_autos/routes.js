const express = require('express');
const router = express.Router();
const getRandomInt = require('../ejemplo_express/aleatorio')

let vehiculos  = require('./vehiculos');

/**
 * @swagger
 * /vehiculos:
 *  get:
 *    description: lista todos los vehiculos
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/vehiculos', (req, res) => {
  res.json(vehiculos);
});

/**
 * @swagger
 * /vehiculos:
 *  post:
 *    description: crea un vehiculo
 *    parameters:
 *    - name: id
 *      description: Id del vehiculo
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: marca
 *      description: Marca del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: Modelo del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: anio
 *      description: Anio del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: puertas
 *      description: Numero de puertas del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: define si el vehículo está disponible para la venta o no
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/vehiculos', (req, res) => {
  const nuevoVehiculo = req.body;
  nuevoVehiculo.id = getRandomInt(1, 10000);
  vehiculos.push(nuevoVehiculo);
  res.json({msj: `vehículo agregado correctamente`});
});


/**
 * @swagger
 * /vehiculos/{id}:
 *  delete:
 *    description: elimina un vehiculo de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del vehiculo
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/vehiculos/:id', (req, res) => {
  const id_vehiculo= parseInt(req.params.id);
  vehiculos = vehiculos.filter(vehiculo => vehiculo.id != id_vehiculo);
  res.json({msj: `vehículo eliminado correctamente`});
});

/**
 * @swagger
 * /vehiculos:
 *  put:
 *    description: actualiza un vehiculo
 *    parameters:
 *    - name: id
 *      description: Id del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: marca
 *      description: Marca del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: modelo
 *      description: Modelo del vehiculo
 *      in: formData
 *      required: true
 *      type: string
 *    - name: anio
 *      description: Anio del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: puertas
 *      description: Numero de puertas del vehiculo
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: disponible
 *      description: define si el vehículo está disponible para la venta o no
 *      in: formData
 *      required: true
 *      type: boolean
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/vehiculos', (req, res) => {
  id_vehiculo = req.body.id;
  const indiceVehiculo = vehiculos.findIndex(x => x.id == id_vehiculo);

  const object = {
    id: parseInt(req.body.id),
    marca: req.body.marca,
    modelo: req.body.modelo,
    anio: parseInt(req.body.anio),
    puertas: parseInt(req.body.puertas),
    disponible: req.body.disponible
  };
  vehiculos[indiceVehiculo] = object;

  res.json({msj: `vehículo actualizado correctamente`});
});

module.exports = router;