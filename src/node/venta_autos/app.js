const express = require('express');
const app = express();
const port = 3017;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const routes = require('./routes');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API de Vehículos en venta',
      version: '1.0.0'
    }
  },
  apis: ['./vehiculos.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

//rutas
app.use('/', routes);


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});