let vehiculos = [
  {id: 1, marca: 'Fiat', modelo: 'Uno', anio: 1999, puertas: 4, disponible: true},
  {id: 2, marca: 'volkswagen', modelo: 'gol', anio: 1999, puertas: 5, disponible: false},
  {id: 3, marca: 'Chevrolet', modelo: 'spark GT', anio: 2001, puertas: 5, disponible: true}
]

module.exports = vehiculos;
