const express = require('express');
const app = express();
const port = 3010;

const cursos = [
  {
    nombre: 'desarrollo web Backend',
    sprints: 3,
    url: 'http://cursos/dwbe',
  },
  {
    nombre: 'desarrollo web Frontend',
    sprints: 3,
    url: 'http://cursos/dwfe',
  },
  {
    nombre: 'desarrollo web FullStack',
    sprints: 4,
    url: 'http://cursos/dwfs',
  },
  {
    nombre: 'DevOps',
    sprints: 2,
    url: 'http://cursos/devops',
  }
];

const logger = (req, res, next) => {
  console.log(req.path);
  next();
};

const validarCurso = (id) => {
  return id < cursos.length;
}

app.use(logger);

app.get('/cursos', function (req, res) {
  res.json(cursos);
});

app.get('/cursos/:id', function (req, res, next) {
  const id = req.params.id-1;
  const CourseExists = validarCurso(id);
  CourseExists ? res.json(cursos[id]) : res.json({msg: `No hay un curso con el ID ${id} `});
});

app.listen(port, function () {
  console.log(`Server listening on port http://localhost:${port}`);
});