const express = require('express');
const app = express();
const port = 3019;
const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const rutasUsuarios = require('./routes/usuarios');
const rutasTopicos = require('./routes/topicos');
const rutasComentarios = require('./routes/comentarios');

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API de foro',
      version: '1.0.0'
    }
  },
  apis: ['./routes/usuarios.js','./routes/topicos.js','./routes/comentarios.js'],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

//rutas
app.use('/usuarios', rutasUsuarios);
app.use('/topicos', rutasTopicos);
app.use('/comentarios', rutasComentarios);


app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});