const express = require('express');
const router = express.Router();
const getRandomInt = require('../../ejemplo_express/aleatorio')

let topicos  = require('../models/topicos');

/**
 * @swagger
 * /topicos/:
 *  get:
 *    tags:
 *    - Topicos
 *    description: lista todos los topicos
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/', (req, res) => {
  res.json(topicos);
});

/**
 * @swagger
 * /topicos:
 *  post:
 *    tags:
 *    - Topicos
 *    description: crea un topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: titulo
 *      description: titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: descripcion del topico
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
  const nuevoTopico = req.body;
  nuevoTopico.id = getRandomInt(1, 10000);
  topicos.push(nuevoTopico);
  res.json({msj: `topico agregado correctamente`});
});


/**
 * @swagger
 * /topicos/{id}:
 *  delete:
 *    tags:
 *    - Topicos
 *    description: elimina un topico de acuerdo a su id
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
  const id_topico= parseInt(req.params.id);
  topicos = topicos.filter(topico => topico.id != id_topico);
  res.json({msj: `topico eliminado correctamente`});
});

/**
 * @swagger
 * /topicos:
 *  put:
 *    tags:
 *    - Topicos
 *    description: actualiza un topico
 *    parameters:
 *    - name: id
 *      description: Id del topico
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: titulo
 *      description: titulo del topico
 *      in: formData
 *      required: true
 *      type: string
 *    - name: descripcion
 *      description: descripcion del topico
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
  id_topico = req.body.id;
  const indicetopico = topicos.findIndex(x => x.id == id_topico);

  const object = {
    id: parseInt(req.body.id),
    titulo: req.body.titulo,
    descripcion: req.body.descripcion
  };
  topicos[indicetopico] = object;

  res.json({msj: `topico actualizado correctamente`});
});

module.exports = router;