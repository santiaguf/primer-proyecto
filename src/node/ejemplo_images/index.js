const coolImages = require("cool-images");
const chalk = require('chalk');

const archivos = require('../ejemplo_archivo/archivos');
//imagen aleatoria
console.log(chalk.blueBright.bold(coolImages.one()));

//10 imágenes
const tenPhotos = coolImages.many(500,300,10);
tenPhotos.forEach(element => {
  console.log(chalk.blue.underline(element));
});

tenPhotos.forEach(function(value, index){
  archivos.write("./src/node/ejemplo_images/log.txt", `img ${index} ${value}`);
});