require('dotenv').config();
const calculadora = require('./calculator');
const chalk = require('chalk');

console.log(chalk.red(`el host de la bd es: ${process.env.DB_HOST}`));

let myArgs = process.argv.slice(2);

const n1 = parseInt(myArgs[0]);
const n2 = parseInt(myArgs[1]);

calculadora.suma(n1,n2);
calculadora.resta(n1,n2);
calculadora.multiplicacion(n1,n2);
calculadora.division(n1,n2);