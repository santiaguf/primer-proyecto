const chalk = require('chalk');

const suma  = (n1, n2) => {
  const result = n1 + n2;
  console.log(chalk.blue(`${n1} + ${n2} = ${result}`));
}

const resta  = (n1, n2) => {
  const result = n1 - n2;
  console.log(chalk.greenBright(`${n1} - ${n2} = ${result}`));
}

const multiplicacion = (n1, n2) => {
  const result = n1 * n2;
  console.log(chalk.yellow(`${n1} * ${n2} = ${result}`));
}

const division = (n1, n2) => {
  if(n2 == 0) {
    console.log(chalk.red('No se puede dividir por 0'));
  }else {
    const result = n1 / n2;
    console.log(chalk.blueBright(`${n1} / ${n2} = ${result}`));
  }
}

exports.suma = suma;
exports.resta = resta;
exports.multiplicacion = multiplicacion;
exports.division = division;