const express = require('express');
const router = express.Router();
/**
 * @swagger
 * /students:
 *  get:
 *    description: lista todos los estudiantes estudiante
 *    responses:
 *      200:
 *        description: Success
 */
 router.get('/', (req, res) => {
   console.log('listar estudiantes');
  res.status(201).send();
});


/**
 * @swagger
 * /students/{id}:
 *  get:
 *    description: Obtiene un estudiante
 *    parameters:
 *    - name: id
 *      description: Id del estudiante
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.get('/1', (req, res) => {
  console.log('student 1');
  const response = { msj: 'student 1'}
 res.status(201).send(response);
});

module.exports = router;