const express = require('express');
const app = express();
const puerto = 3004;

const telefonos = [
{
    marca: 'Samsung',
    gama: 'alta',
    modelo: 'Galaxy S7',
    precio: 999,
    pantalla: '6',
    sistema_operativo : 'Android'
},
{
    marca: 'Apple',
    gama: 'alta',
    modelo: 'iPhone 12',
    precio: 1299,
    pantalla: '6',
    sistema_operativo: 'iOS'
},
{
    marca: 'Xiaomi',
    gama: 'media',
    modelo: 'Redmi Note 3',
    precio: 399,
    pantalla: '4',
    sistema_operativo: 'Android'
},
{
    marca: 'Realme',
    gama: 'media',
    modelo: '7',
    precio: 244,
    pantalla: '5',
    sistema_operativo: 'Android'
},
{
  marca: 'Sony Ericsson',
  gama: 'alta',
  modelo: 'k850',
  precio: 544,
  pantalla: '5',
  sistema_operativo: 'Java'
},
{
  marca: 'nokia',
  gama: 'baja',
  modelo: '1100',
  precio: 100,
  pantalla: '5',
  sistema_operativo: 'Java'
}];

const logger = (req, res, next) => {
  console.log(`soy un logger y muestro información `);
  console.log(`${req.method} ${req.url} path: ${req.path} `);
  next();
}

const mostrarOferta = (req, res, next) => {
  console.log(`estos teléfonos están en oferta con el 10% si lo compras en los próximos 5 minutos`);
  next();
}

const clienteGamaAlta = () => {
  console.log(`te regalamos los earpods si compras el iphone `);
}

const esAdministrador = (req, res, next) => {
  const admin = true;
  if(admin){
    console.log(`logueado correctamente`);
  }else{
    console.log(`Usuario tramposo`);
    res.send('no estás logeado');
    return;
  }
  next();
}

//middleware global
app.use(logger);

const telefonosOrdenados = telefonos.sort(function (a, b) {
  return a.precio > b.precio ? 1 : -1
});

const gamaAlta = telefonos.filter(telefono => telefono.gama === 'alta');
const gamaMedia = telefonos.filter(telefono => telefono.gama === 'media');
const gamaBaja = telefonos.filter(telefono => telefono.gama === 'baja');
const filtrados = [gamaAlta, gamaMedia, gamaBaja];

app.get('/telefonos',esAdministrador, (req, res) => {
  res.json(telefonos);
});

app.get('/gamaAlta', mostrarOferta, (req, res) => {
  clienteGamaAlta();
  res.json(gamaAlta);
});

app.get('/gamaMedia', mostrarOferta, (req, res) => {
  res.json(gamaMedia);
});

app.get('/gamaBaja', mostrarOferta, (req, res) => {
  res.json(gamaBaja);
});

app.get('/mitad', (req, res) => {
  res.json(telefonos.slice(0, telefonos.length / 2));
});

app.get('/masBarato', (req, res) => {
  res.json(telefonosOrdenados[0]);
});

app.get('/masCaro', (req, res) => {
  res.json(telefonosOrdenados[telefonosOrdenados.length-1]);
});

app.get('/filtrados', (req, res) => {
  res.json(filtrados);
});



app.listen(puerto, () => {
  console.log(`Servidor corriendo en http://localhost:${puerto}`);
});