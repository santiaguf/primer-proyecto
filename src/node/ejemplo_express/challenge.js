const express = require('express')
const app = express()
const port = 3001;

let trainees = [{
    nombre: "leonel",
    apellido: "Messi",
    ciudad: "Barcelona"
},{
    nombre: "Cristiano",
    apellido: "Ronaldo",
    ciudad: "Turin"
},{
    marca: "neymar",
    modelo: "da silva",
    ciudad: "Paris"
},{
    marca: "Erling",
    modelo: "Halaand",
    ciudad: "Dortmund"
}]

app.get('/', function (req, res) {
    res.send(`hello world, <br />just try <strong>/trainees</strong> or <strong>/trainees/1</strong>`);
})

app.get('/trainees', function (req, res) {
  res.json(trainees);
})

app.get('/trainees/:traineeId', function (req, res) {
    traineeId = req.params.traineeId;
    res.json(trainees[traineeId]);
  })

app.listen(port, function() {
    console.log(`Express corriendo en http://localhost:3001`)
})