function hablar(algo){
  console.log(algo);
}

function hablar_bajito(algo){
  console.log(algo.toLowerCase());
}

function gritar(algo){
  console.log(algo.toUpperCase());
}

exports.hablar = hablar;
exports.hablar_bajito = hablar_bajito;
exports.gritar = gritar;