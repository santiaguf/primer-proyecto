const express = require('express');
const funciones = require('./funciones');
const app = express();
const port = 3011;
const host = 'http://localhost';
const url = `${host}:${port}`;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const logger = (req, res, next) => {
  console.log(`${req.method} ${req.url} path: ${req.path} `);
  next();
}

app.use(logger);

app.get('/autores', (req, res) => {
  let respuesta = {};
  respuesta.msj = funciones.mostrarAutores();
  res.json(respuesta);
});

app.post('/autores', (req, res) => {
  let respuesta = {};
  respuesta.msj = funciones.crearAutor(req.body);
  res.json(respuesta);
});

app.put('/autores/:id', (req, res) => {
  const id_autor= req.params.id;
  let respuesta = {};
  respuesta.msj = funciones.idExiste(id_autor) ? funciones.actualizarAutor(id_autor, req.body) : `ese autor no existe, puede ver todos los autores en ${url}/autores`;
  res.json(respuesta);
});

app.get('/autores/:id', (req, res) => {
  const id_autor= req.params.id;
  let respuesta = {};

  respuesta.msj = funciones.idExiste(id_autor) ? funciones.buscarAutor(id_autor) : `ese autor no existe, puede ver todos los autores en ${url}/autores`;

  res.json(respuesta);
});

app.delete('/autores/:id', (req, res) => {
  const id_autor= req.params.id;
  let respuesta = {};
  respuesta.msj = funciones.idExiste(id_autor) ? funciones.borrarAutor(id_autor) : `ese autor no existe, puede ver todos los autores en ${url}/autores`;
  res.json(respuesta);
});

app.get('/autores/:id/libros', (req, res) => {
  const id_autor= req.params.id;
  let respuesta = {};

  respuesta.msj = funciones.mostrarLibrosDeAutor(id_autor);
  res.json(respuesta);
});

app.post('/autores/:id/libros', (req, res) => {
  const id_autor= req.params.id;
  let respuesta = {};

  respuesta.msj = funciones.crearLibrosdeAutor(id_autor, req.body);
  res.json(respuesta);
});

app.get('/autores/:id/libros/:idLibro', (req, res) => {
  const id_autor= req.params.id;
  const id_libro = req.params.idLibro;
  let respuesta = {};
  respuesta.msj = funciones.mostrarLibroDeAutor(id_autor, id_libro);
  res.json(respuesta);
});

app.put('/autores/:id/libros/:idLibro', (req, res) => {
  const id_autor= req.params.id;
  const id_libro = req.params.idLibro;
  let respuesta = {};
  respuesta.msj = funciones.actualizarLibro(id_autor, id_libro, req.body);
  res.json(respuesta);
});

app.delete('/autores/:id/libros/:idLibro', (req, res) => {
  const id_autor= req.params.id;
  const id_libro = req.params.idLibro;
  let respuesta = {};
  respuesta.msj = funciones.borrarLibro(id_autor, id_libro);
  res.json(respuesta);
});

app.listen(port, () =>{
  console.log(`listening on ${url}`);
});